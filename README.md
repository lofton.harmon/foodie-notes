# Welcome Foodie Note

Installation of the Django back end is very straightforward.
The sqllite database has been included for convience

`virtualenv -p python3 venv`

Source into your virtual environment and then run the following command in the root folder of the project.

`pip install -r requirements` 

Lastly if you wanna get into the backend you'll need to register as a super user

`./manage.py createsuperuser`

To Run tests use the following command.
Current tests implemented only test CRUD operations
and
Model operations responsible for calculating the user_rating on Restaurants

`./manage.py test`