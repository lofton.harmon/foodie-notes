from django.urls import path, include
from restaurants.views import (
    RestaurantViewset, 
    FoodTypeViewset, 
    ReviewViewset
)
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'restaurants', RestaurantViewset)
router.register(r'food-types', FoodTypeViewset)
router.register(r'reviews', ReviewViewset)

urlpatterns = [
    path('', include(router.urls)),
]