from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator


class Restaurant(models.Model):
    name = models.CharField(max_length=60)
    address = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255, null=True, default=None)
    state_province = models.CharField(max_length=60)
    postal_code = models.CharField(max_length=5)
    country = models.CharField(max_length=60)
    description = models.TextField(max_length=800)
    food_type = models.ForeignKey('FoodType', on_delete=models.SET_NULL, default=None, null=True)
    user_rating = models.PositiveIntegerField(
        default=0,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(5)
        ]
    )

    def getAverageRating(self):
        return int(self.reviews.all().aggregate(models.Avg('rating'))['rating__avg'])



    def __str__(self):
        return f'Restaurant {self.name}'

class FoodType(models.Model):
    name = models.CharField(max_length=60)
    slug = models.SlugField(max_length=60)

    def __str__(self):
        return f'FoodType {self.name}'


class Review(models.Model):
    body = models.TextField(max_length=800)
    restaurant = models.ForeignKey(
        Restaurant, 
        on_delete=models.CASCADE, 
        default=None, 
        null=True, 
        related_name="reviews"
    )
    rating = models.PositiveIntegerField(
        default=0,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(5)
        ]
    )


    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.restaurant.user_rating = self.restaurant.getAverageRating()
        self.restaurant.save()

    def __str__(self):
        return f'Review for restaurant {self.restaurant.name}'