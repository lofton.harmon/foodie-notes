from rest_framework import viewsets
from restaurants.models import Restaurant, FoodType, Review
from restaurants.serializers import RestaurantSerializer, FoodTypeSerializer, ReviewSerializer

class RestaurantViewset(viewsets.ModelViewSet):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()


class FoodTypeViewset(viewsets.ReadOnlyModelViewSet):
    serializer_class = FoodTypeSerializer
    queryset = FoodType.objects.all()

class ReviewViewset(viewsets.ModelViewSet):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()