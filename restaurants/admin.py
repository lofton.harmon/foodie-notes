from django.contrib import admin
from restaurants.models import Review, Restaurant, FoodType

class FoodTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}



admin.site.register(FoodType, FoodTypeAdmin)
admin.site.register(Restaurant)
admin.site.register(Review)