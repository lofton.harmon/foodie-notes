from rest_framework import serializers
from restaurants.models import Restaurant, FoodType, Review

class FoodTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodType
        fields = '__all__'


class RestaurantSerializer(serializers.ModelSerializer):
    food_type_data = FoodTypeSerializer(source="food_type", read_only=True)

    
    class Meta:
        model = Restaurant
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Review
        fields = '__all__'