from django.test import TestCase
from restaurants.models import Review, Restaurant, FoodType


class TestModels(TestCase):
    def setUp(self):
        self.testaurant = Restaurant.objects.create(
            food_type=None,
            name="Testaurant",
            address="123 Main street",
            address2="suite 25",
            state_province="Utah",
            postal_code="84045",
            country="US",
            description="This is just a test restaruant",
            user_rating=5
        )

        self.review1 = Review.objects.create(
            body="this is a test review",
            restaurant=self.testaurant,
            rating=3
        )

        self.review2 = Review.objects.create(
            body="this is a test review",
            restaurant=self.testaurant,
            rating=5
        )

    def test_restraunt_updates_user_rating_when_review_is_saved(self):
        self.testaurant.refresh_from_db()
        self.assertEquals(self.testaurant.user_rating, 4)

    def test_restraunt_updates_user_rating_when_review_is_saved_again(self):
        Review.objects.create(
            body="this is a test review",
            restaurant=self.testaurant,
            rating=3
        )
        self.testaurant.refresh_from_db()
        self.assertEquals(self.testaurant.user_rating, 3)

