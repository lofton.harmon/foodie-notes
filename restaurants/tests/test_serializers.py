from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from restaurants.models import FoodType, Restaurant, Review


class FoodTypeTests(APITestCase):
    def setUp(self):
        self.food_type_test = FoodType.objects.create(name="test", slug="test")

    def test_create_food_type(self):
        """
        Ensure we can not create a FoodType as it's read only
        """
        url = reverse('foodtype-list')
        data = {
            "name": "FooBar",
            "slug": "foo-bar"
        }
        response = self.client.post(url, data, format='json')
        
        # Make sure we get a method not allowed
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


    def test_get_food_type(self):
        """
        Ensure we can fetch a FoodType
        """
        url = reverse('foodtype-detail', kwargs={"pk": self.food_type_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_update_food_type(self):
        """
        Ensure we can not update a FoodType.
        """
        url = reverse('foodtype-detail', kwargs={'pk': self.food_type_test.id})
        data = {'name': 'Foo Bar'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_restaurant(self):
        """
        Ensure we cannot delete a restaurant
        """
        url = reverse('foodtype-detail', kwargs={'pk': self.food_type_test.id})
        response = self.client.delete(url)
        
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_list_food_types(self):
        """
        Ensure we can list out our food types
        """
        url = reverse('foodtype-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class RestruantTests(APITestCase):
    def setUp(self):
        self.food_type_test = FoodType.objects.create(name="test", slug="test")
        self.testaurant = Restaurant.objects.create(
            food_type=self.food_type_test,
            name="Testaurant",
            address="123 Main street",
            address2="suite 25",
            state_province="Utah",
            postal_code="84045",
            country="US",
            description="This is just a test restaruant",
            user_rating=5
        )

    def test_create_restaurant(self):
        """
        Ensure we can create a restaurant
        """
        url = reverse('restaurant-list')
        data = {
            "food_type": self.food_type_test.id,
            "name": "Testaurant",
            "address": "123 Main street",
            "address2": "suite 25",
            "state_province": "Utah",
            "postal_code": "84045",
            "country": "US",
            "description": "This is just a test restaruant",
            "user_rating": 3
        }
        response = self.client.post(url, data, format='json')
        
        # Make sure we are getting a 201 back
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Iterate over passed in variables and assert they are equal
        for key, value in data.items():
            # Edgecase, Food type comes back as a serialized sub object, search it for id
            if key == 'food_type_id' and data['food_type']:
                self.assertEqual(response.data['food_type']['id'], value)
            else:
                self.assertEqual(response.data[key], value)


    def test_get_restaurant(self):
        """
        Ensure we can fetch a Restaurant
        """
        url = reverse('restaurant-detail', kwargs={"pk": self.testaurant.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_update_restaurant(self):
        """
        Ensure we can update a restaurant.
        """
        url = reverse('restaurant-detail', kwargs={'pk': self.testaurant.id})
        data = {'name': 'Foo Bar'}
        response = self.client.patch(url, data, format='json')
        
        # Make sure we exceute the request successfully then compare the names
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], data['name'])

    def test_delete_restaurant(self):
        """
        Ensure we can delete a restaurant
        """
        test_id = self.testaurant.id
        url = reverse('restaurant-detail', kwargs={'pk': test_id})
        response = self.client.delete(url)

        # on a successfull delete DRF will return a 204
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # double check and make sure the record was deleted
        self.assertFalse(response.data, Restaurant.objects.filter(id=test_id).exists())
    
    def test_list_restaurants(self):
        """
        Ensure we can list out the restaurants
        """
        url = reverse('restaurant-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)




class ReviewTests(APITestCase):
    def setUp(self):
        self.food_type_test = FoodType.objects.create(name="test", slug="test")
        self.testaurant = Restaurant.objects.create(
            food_type=self.food_type_test,
            name="Testaurant",
            address="123 Main street",
            address2="suite 25",
            state_province="Utah",
            postal_code="84045",
            country="US",
            description="This is just a test restaruant",
            user_rating=5
        )
        self.review = Review.objects.create(
            body="this is a test review",
            restaurant=self.testaurant,
            rating=3
        )

    def test_create_review(self):
        """
        Ensure we can create review
        """
        url = reverse('review-list')
        data = {
            "body": "Foobar",
            "restaurant": self.testaurant.id,
            "rating": 5
        }
        response = self.client.post(url, data, format='json')
        
        # Make sure we get a method not allowed
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Compare reqeust body and response except for the ID
        del response.data['id']
        self.assertEqual(response.data, data)


    def test_get_review(self):
        """
        Ensure we can fetch a review
        """
        url = reverse('review-detail', kwargs={"pk": self.review.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_update_review(self):
        """
        Ensure we can  update a Review.
        """
        url = reverse('review-detail', kwargs={'pk': self.review.id})
        data = {'body': 'Super Great French Fries'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['body'], data['body'])

    def test_delete_review(self):
        """
        Ensure we can delete a Review
        """
        test_id = self.review.id
        url = reverse('review-detail', kwargs={'pk': test_id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Review.objects.filter(id=test_id).exists())

    def test_list_review(self):
        """
        Ensure we can list all the reviews
        """
        url = reverse('review-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)